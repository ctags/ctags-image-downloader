const puppeteer = require("puppeteer");

let browser;
let page;

(async () => {
  
    browser = await puppeteer.launch({ headless: false });
    page = await browser.newPage();
    page.setViewport({ width: 2560, height: 1440 });
    await page.goto("http://localhost:3000/", { waitUntil: "networkidle0" });

    var imageCount = await page.evaluate(doc =>
        {
            var header = document.getElementById("header");

            return header.children.length;
        });

    for (let i = 0; i < imageCount; i++)
    {
        let index = i + 1;

        var selector = "#header > div:nth-child(" + index + ") > div";
        var child = await page.$(selector);
        var name = await child.getProperty("id");

        var name = await page.evaluate(({selector}) => {
            return document.querySelector(selector).id;
        }, {selector});


        console.log(name);

        await child.screenshot({path: (name + ".jpg")})
    }

  
    
    await browser.close();
  })();
  